package prod;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class ProductRepository {



        private EntityManager entityManager;

        public ProductRepository(EntityManager entityManager) {
            this.entityManager = entityManager;
        }

        public void addItem(Produkt produkt) {
            entityManager.getTransaction().begin();
            entityManager.persist(produkt);
            entityManager.getTransaction().commit();
        }



        public List<Produkt> showProducts(){

            TypedQuery<Produkt>query = entityManager.createQuery("From Produkt",Produkt.class);
            return query.getResultList();
        }
    }

