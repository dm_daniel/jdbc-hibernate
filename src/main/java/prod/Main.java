package prod;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static ProductRepository productRepository;

    public static void main(String[] args) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("produkty");
        EntityManager entityManager = factory.createEntityManager();

        System.out.println("1. Dodaj produkt");
        System.out.println("2. Wyświetl produkty");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        productRepository = new ProductRepository(entityManager);

        switch (choice) {
            case 1:

                Produkt produkt = zapytajOProdukt();
                productRepository.addItem(produkt);
                break;
            case 2:
                List<Produkt> listaProduktow;
                listaProduktow = productRepository.showProducts();
                listaProduktow.forEach(System.out::println);


        }


        entityManager.close();
        factory.close();
    }

    public static Produkt zapytajOProdukt() {
        Produkt produkt = new Produkt();
        Scanner scanner = new Scanner(System.in);

        System.out.println("podaj nazwę produktu: ");
        produkt.nazwa = scanner.nextLine();

        System.out.println("podaj kategorię produktu: ");
        produkt.kategoria = scanner.nextLine();

        System.out.println("podaj cenę produktu: ");
        produkt.cena = scanner.nextBigDecimal();

        System.out.println("podaj ocenę produktu: ");
        produkt.ocena = scanner.nextInt();

        System.out.println("podaj opis produktu: ");
        produkt.opis = scanner.nextLine();

        return produkt;

    }


}
